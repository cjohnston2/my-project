export const matchGitlabIssue = [
  /^https:\/\/gitlab\..*?com\/(?<groupName>[^/]+)\/(?<projectName>[^/]+)\/-\/issues\/(?<id>\d+)/,
];

export const matchGitlabMergeRequest = [
  /^https:\/\/gitlab\..*?com\/(?<groupName>[^/]+)\/(?<projectName>[^/]+)\/-\/merge_requests\/(?<id>\d+)/,
];

export const matchGitlabProject = [
  /^https:\/\/gitlab\..*?com\/(?<groupName>[^/]+)\/(?<projectName>[^/]+)\/?$/,
  /^https:\/\/gitlab\..*?com\/(?<groupName>[^/]+)\/(?<projectName>[^/]+)\/activity/,
  /^https:\/\/gitlab\..*?com\/(?<groupName>[^/]+)\/(?<projectName>[^/]+)\/-\/releases/,
];
