import { ObjectResolver, Client } from '@atlassian/forge-object-resolver';
import { getBranch, getDetailedRepo, PatchedReposGetResponse, PatchedReposGetBranchResponse } from './requests';
import { BranchMatch } from '../patterns/types';

export type ResolveGithubBranchResponse = [PatchedReposGetResponse, PatchedReposGetBranchResponse];

export const resolveGithubBranch: ObjectResolver = async function resolveGithubBranch(
  client: Client,
  _url: string,
  { ownerName, repoName, branchName }: BranchMatch,
): Promise<ResolveGithubBranchResponse> {
  return await Promise.all([
    getDetailedRepo(client, ownerName, repoName),
    getBranch(client, ownerName, repoName, branchName),
  ]);
};
